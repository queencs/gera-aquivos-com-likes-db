import java.sql.Statement;
import java.util.ArrayList;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.TimeoutException;

public class GerarArquivo {

	public static void main(String[] args){
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://evaacollector.c8jcboubgzkm.sa-east-1.rds.amazonaws.com:3306/eva_collector?useSSL=false&useTimezone=true&serverTimezone=UTC","root","Fag120dto");


			//A) Construir o cabelçalho dos dados

			String sqlCabcalho = "SELECT DISTINCT "+
					"curtida "+
					"FROM "+
					" eva_collector.profile_profile pp "+
					" INNER JOIN"+
					" eva_collector.profile_likes pl "+
					"ON pl.id_profile = pp.link "+
					"WHERE "+
					" pp.id_collect = 160 "+
					"group by curtida "+
					"ORDER BY COUNT(curtida) DESC "+
					"LIMIT 50;";

			Statement comando = con.createStatement();

			ResultSet resCabcalho = comando.executeQuery(sqlCabcalho);
			ArrayList<String>Header = new ArrayList<String>();
			System.out.println("Chegou aqui ");
			while(resCabcalho.next()) {
				Header.add(resCabcalho.getString("curtida"));
			}

			//B) Construir a lista de perfis
			String sqlPerfis ="SELECT  distinct link "+
					"FROM "+
					"eva_collector.profile_profile " +
					"WHERE "+
					"id_collect = 160;";
			ResultSet resPerfis = comando.executeQuery(sqlPerfis);
			ArrayList<String>Perfis = new ArrayList<String>();
			int cont = 0 ;
			while(resPerfis.next()) {
				cont++;
				System.out.println(cont);
				Perfis.add(resPerfis.getString("link"));
			}

			//C Construir a matriz com os dados
			String[][] dados = new String[Perfis.size()][Header.size()];
			//Navegar em cada perfil e selecionar os times curtidos pelo perfil
			resPerfis.first();
			int linhadados= 0;
/*			sqlPerfis = "select distinct perfil from perfil_time order by perfil;";
			resPerfis = comando.executeQuery(sqlPerfis);*/
			
			for(String perfil : Perfis) {//Para cada perfil, buscar os times curtidos

				String sqlPerfilTime = "SELECT distinct " +
						"   pl.curtida " +
						"FROM " +
						"    eva_collector.profile_profile pp " +
						"        INNER JOIN " +
						"    eva_collector.profile_likes pl ON pl.id_profile = pp.link " +
						"WHERE " +
						"    pl.id_profile = '"+perfil+"'" +
						"        AND pp.id_collect = 160 " +
						"ORDER BY curtida;";

				ResultSet resPerfilTime = comando.executeQuery(sqlPerfilTime);
				while(resPerfilTime.next()) {//Para cada time que retornar no perfil atual, buscar no cabeçalho (Header) e marcar S na posição relativa
					for(int i =0; i<Header.size();i++) {
						if(Header.get(i).contains(resPerfilTime.getString("curtida"))) {

							dados[linhadados][i] = resPerfilTime.getString("curtida").replaceAll(" ","_"); //Pode-se substirui o for pelo Header.indexOf() -- Opcional, mas acelera a consulta
						}
					}
				}
				linhadados++;	
			}
			
			//D Gravar o arquivo que será processado no Weka
			FileWriter arquivo = new FileWriter("files/SankeyLikes.csv");
			BufferedWriter gravador = new BufferedWriter(arquivo);
			String linha = ""; //String que será gravada em cada linha do arquivo de saída
			//Montar o cabeçalho do arquivo de saída (processado no Weka)
			//Navegar no ArrayList Header para montar a primeira linha
	/*		int i = 0;
			for(String s : Header) {
				
				if(i<Header.size()-1) {
					linha = linha + s+","; 
					i++;
				}
				else 
				{
					linha = linha + s + System.lineSeparator();
					i++;
				}
			}
			//Grava o cabeçalho
			gravador.write(linha);
			linha = "";*/
			//Navegar na matriz dados
			for(int j = 0; j<dados.length;j++) {
				for(int k = 0;k<dados[j].length;k++) {

					System.out.println(j+" "+k);
					if(k<dados[j].length-1) {
						linha = linha + dados[j][k]+" ";
						
					}
					else 
					{
						linha = linha + dados[j][k]+ System.lineSeparator();

					}
				}
			}
			gravador.write(linha.replaceAll("null"," "));
			gravador.flush();
			gravador.close();
			
			System.out.println("Foi");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
